package com.company.human;

import com.company.pet.Pet;

import java.util.HashMap;

public final class Woman extends Human {
    public Woman(){
        super();
    }
    public Woman(String name, String surname, int year, int id){
        super(name, surname, year, id);
    }
    public Woman(String name, String surname, int year, int id, HashMap<String, String> schedule){
        super(name, surname, year, id, schedule);
    }

    {

    }

    @Override
    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) System.out.println("Сейчас мы тебя накрасим, " + pet.getNickname());
            }
        }
    }
    @Override
    public void makeup() {
        System.out.println("*звуки красящийся женщины*");
    }
}

package com.company.human;

import com.company.pet.Pet;

import java.util.Arrays;
import java.util.HashMap;

public final class Man extends Human {
    public Man(){
        super();
    }
    public Man(String name, String surname, int year, int id){
        super(name, surname, year, id);
    }
    public Man(String name, String surname, int year, int id, HashMap<String, String> schedule){
        super(name, surname, year, id, schedule);
    }

    {

    }

    @Override
    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) System.out.println("Даров епта, " + pet.getNickname());
            }
        }
    }
    @Override
    public void repairCar() {
        System.out.println("чинит машину...");
    }
}

package com.company.family;

import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;
import com.company.pet.Pet;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class FamilyService {
    CollectionFamilyDao FamilyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return FamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        for(Family family : FamilyDao.getAllFamilies()){
            System.out.print(family.toString()+"  ░▒▓▒░  ");
        }
        System.out.println();
    }


    public void getFamiliesBiggerThan(int count) {
        for (Family family : FamilyDao.getAllFamilies()) {
            if (count < family.countFamily()) {
                System.out.print(family.toString()+"  ░▒▓▒░  ");
            }
        }
        System.out.println();
    }


    public void getFamiliesLessThan(int count) {
        for(Family family : FamilyDao.getAllFamilies()){
            if(count > family.countFamily()){
                System.out.print(family.toString()+"  ░▒▓▒░  ");
            }
        }
        System.out.println();
    }


    public int countFamiliesWithMemberNumber(int count) {
        int countf = 0;
        for(Family family : FamilyDao.getAllFamilies()){
            if(count == family.countFamily()){
                countf++;
            }
        }
        return countf;
    }


    public void createNewFamily(Human parent1, Human parent2) {
        Family family = new Family(parent1, parent2);
        FamilyDao.saveFamily(family);
    }


    public Family getFamilyById (int index) {
        return FamilyDao.getFamilyByIndex (index);
    }


    public void deleteFamilyByIndex(int index) {
        FamilyDao.deleteFamily(index);
    }

    //оставил чтоб было)
    public void deleteFamilyByFamily(Family family) {
        FamilyDao.deleteFamily(family);
    }


    public Family bornChild(Family family, String nameM, String nameW) {
        Human child;
        Random random = new Random();
        if(random.nextInt(2) == 1){
            child = new Man();
            child.setName(nameM);
        }
        else {
            child = new Woman();
            child.setName(nameW);
        }
        family.addChild(child);
        FamilyDao.saveFamily(family);
        return family;
    }


    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        FamilyDao.saveFamily(family);
        return family;
    }


    public void deleteAllChildrenOlderThen(int year) {
        for(Family family : FamilyDao.getAllFamilies()){
            family.getChildren().removeIf(human -> human.getYear() > year);
            FamilyDao.saveFamily(family);
        }
    }


    public int count() {
        return FamilyDao.getAllFamilies().size();
    }



    public HashSet<Pet> getPets(int index) {
        int in = 0;
        for(Family family : FamilyDao.getAllFamilies()){
            if(in == index){
                return family.getPet();
            }
            in++;
        }
        return null;
    }


    public void addPet(int index, Pet pet) {
        int in = 0;
        for(Family family : FamilyDao.getAllFamilies()){
            if(in == index){
                family.addPet(pet);
                FamilyDao.saveFamily(family);
                break;
            }
            in++;
        }
    }





//for test)
    public void saveFamily(Family family) {
        FamilyDao.saveFamily(family);
    }


}

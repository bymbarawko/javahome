package com.company.family;


import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }


    @Override
    public Family getFamilyByIndex (int index) {
        int in = 0;
        for(Family family : familyList){
            if(in == index) {
                return family;
            }
            in++;
        }
        return null;
    }



    @Override
    public boolean deleteFamily(int index) {
        try {
            familyList.remove(index);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        int in = 0;
        for(Family familythis : familyList){
            if(familythis == family) {
                familyList.remove(in);
                return true;
            }
            in++;
        }
        return false;
    }

    @Override
    public void saveFamily(Family family) {
        boolean famtry = false;
        int in = 0;
        for(Family familythis : familyList){
            if(familythis.equals(family)){
                familyList.set(in, family);
                famtry = true;
            }
            in++;
        }
        if (!famtry)
        familyList.add(family);
    }
}

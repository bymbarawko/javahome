package com.company.pet;

import java.util.HashSet;

public class Fish extends Pet {
    private Species species;
    public Fish () {
        super();
    }
    public Fish (String nickname) {
        super(nickname);
    }
    public Fish (String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    {

    }

    @Override
    public void respond() {
        System.out.println("буль буль хлюп буль");
    }
}

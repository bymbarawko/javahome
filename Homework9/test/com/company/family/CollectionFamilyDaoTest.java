package com.company.family;

import com.company.human.Man;
import com.company.human.Woman;
import org.junit.Test;

import static org.junit.Assert.*;

public class CollectionFamilyDaoTest {

    @Test
    public void getAllFamilies() {
    }

    @Test
    public void getFamilyByIndex() {
    }

    @Test
    public void deleteFamily() {
    }

    @Test
    public void testDeleteFamily() {
    }

    @Test
    public void saveFamily() {
        CollectionFamilyDao famDB = new CollectionFamilyDao();
        Woman mother = new com.company.human.Woman("Vasilisa", "levchenko", 23, 90);
        Man father = new com.company.human.Man("Sasha", "levchenko", 25, 90);

        Woman mother1 = new com.company.human.Woman("Vasilisa", "levchenko", 23, 70);
        Man father1 = new com.company.human.Man("Sasha", "levchenko", 25, 90);

        Family smist = new Family(mother, father);
        Family smist1 = new Family(mother1, father1);

        famDB.saveFamily(smist);
        famDB.saveFamily(smist1);
        smist1.bornChild();
        smist1.bornChild();
        smist1.bornChild();
        famDB.saveFamily(smist1);
        assertEquals(famDB.getAllFamilies().toArray().length, 2);

    }
}
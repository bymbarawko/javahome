package com.company.family;

import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;
import com.company.pet.Dog;
import com.company.pet.Pet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class FamilyServiceTest {

    @Test
    public void getAllFamilies() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        List<Family> actual = new ArrayList<>();
        actual.add(smits);
        actual.add(griffins);
        List<Family> expected = familyBD.getAllFamilies();
        assertEquals(expected, actual);
    }

    @Test
    public void displayAllFamilies() {

    }

    @Test
    public void getFamiliesBiggerThan() {

    }

    @Test
    public void getFamiliesLessThan() {

    }

    @Test
    public void countFamiliesWithMemberNumber() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);

        assertEquals(familyBD.countFamiliesWithMemberNumber(2), 2);
    }

    @Test
    public void createNewFamily() {
        FamilyService familyBD = new FamilyService();
        familyBD.createNewFamily(new Man(), new Woman());

        assertEquals(familyBD.getAllFamilies().size(), 1);

    }

    @Test
    public void deleteFamilyByIndex() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        ArrayList<Family> actual = new ArrayList<>();
        actual.add(griffins);
        familyBD.deleteFamilyByIndex(0);

        assertEquals(familyBD.getAllFamilies(), actual);

    }

    @Test
    public void NotdeleteFamilyByWrongIndex() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        ArrayList<Family> actual = new ArrayList<>();
        actual.add(smits);
        actual.add(griffins);
        familyBD.deleteFamilyByIndex(8);

        assertEquals(familyBD.getAllFamilies(), actual);

    }

    @Test
    public void bornChild() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.bornChild(smits, "vasia", "lesia");

        assertEquals(familyBD.getFamilyById(0).getChildren().size(), 1);
    }

    @Test
    public void adoptChild() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.adoptChild(smits, new Human());

        assertEquals(familyBD.getFamilyById(0).getChildren().size(), 1);
    }

    @Test
    public void deleteAllChildrenOlderThen() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.adoptChild(smits, new Human("vasia", "petrov", 5, 50));
        familyBD.adoptChild(smits, new Human("vasia", "petrov", 10, 50));

        familyBD.deleteAllChildrenOlderThen(7);

        assertEquals(familyBD.getFamilyById(0).getChildren().size(), 1);

    }

    @Test
    public void count() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family smits1 = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(smits1);

        assertEquals(familyBD.count(), 2);
    }

    @Test
    public void deleteFamilyFromIndex() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        ArrayList<Family> actual = new ArrayList<>();
        actual.add(griffins);
        familyBD.deleteFamilyByIndex(0);


        assertEquals(familyBD.getAllFamilies(), actual);
    }

    @Test
    public void NotdeleteFamilyFromWrongIndex() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        ArrayList<Family> actual = new ArrayList<>();
        actual.add(smits);
        actual.add(griffins);
        familyBD.deleteFamilyByIndex(5);

        assertEquals(familyBD.getAllFamilies(), actual);
    }

    @Test
    public void DeleteFamilyFromFamily() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        ArrayList<Family> actual = new ArrayList<>();
        actual.add(griffins);
        familyBD.deleteFamilyByFamily(smits);


        assertEquals(familyBD.getAllFamilies(), actual);
    }

    @Test
    public void NotDeleteFamilyFromWrongFamily() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        Family griffins1 = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        ArrayList<Family> actual = new ArrayList<>();
        actual.add(smits);
        actual.add(griffins);
        familyBD.deleteFamilyByFamily(griffins1);

        assertEquals(familyBD.getAllFamilies(), actual);
    }

    @Test
    public void saveNewFamily() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        assertEquals(familyBD.count(), 2);
    }

    @Test
    public void resaveCurrentFamily() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Family griffins = new Family(new Man(), new Woman());
        familyBD.saveFamily(smits);
        familyBD.saveFamily(griffins);
        griffins.bornChild();
        familyBD.saveFamily(griffins);
        assertEquals(familyBD.count(), 2);
        assertEquals(familyBD.getFamilyById(1), griffins);
    }

    @Test
    public void getPets() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Pet dog = new Dog();
        smits.addPet(dog);
        familyBD.saveFamily(smits);
        HashSet<Pet> petsf = familyBD.getPets(0);
        HashSet<Pet> actual = new HashSet<>();
        actual.add(dog);

        assertEquals(actual, petsf);
    }

    @Test
    public void addPet() {
        FamilyService familyBD = new FamilyService();
        Family smits = new Family(new Man(), new Woman());
        Pet dog = new Dog();
        familyBD.saveFamily(smits);
        familyBD.addPet(0, dog);
        HashSet<Pet> petsf = familyBD.getPets(0);
        HashSet<Pet> actual = new HashSet<>();
        actual.add(dog);

        assertEquals(actual, petsf);
    }
}
package com.company.human;


import com.company.Family;
import com.company.pet.Pet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int id;
    private HashMap<String, String> schedule;
    protected static Family family;

    static {
        System.out.println("загружаеться класс Human");
    }

    {
        System.out.println("создан новый обьект класса: "+this.getClass().getSimpleName());
    }

    public Human() {

    }
    public Human(String name, String surname, int year, int id) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
    }
    public Human(String name, String surname, int year, int id, HashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
        this.schedule = schedule;
    }


    //getters
    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public int getYear() {
        return this.year;
    }
    public int getId() {
        return this.id;
    }
    public HashMap<String, String> getSchedule() {
        return this.schedule;
    }
    public Family getFamily() {
        return this.family;
    }

    //setters(зачеееееем?))
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }
    public void setFamily(Family family) {
        this.family = family;
    }


    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) System.out.println("Привет, " + pet.getNickname());
            }
        }
        else {
            System.out.println("Привет, одиночество))");
        }
    }
    public void describePet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) {
                    if (pet.getTrickLevel() > 50) {
                        System.out.println("У меня есть "+pet.getSpecies()+", ему "+pet.getAge()+" лет, он очень хитрый");
                    }
                    else {
                        System.out.println("У меня есть "+pet.getSpecies()+", ему "+pet.getAge()+" лет, он почти не хитрый");
                    }
                }
            }
        }

        else {
            System.out.println("У меня нет питомца.");
        }
    }
    void makeup() {

    }
    void repairCar() {

    }




    @Override
    public String toString() {
            return  "Пол= "+this.getClass().getSimpleName()+" "+"name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", year=" + year +
                    ", id=" + id +  "schedule" +schedule;
    }



    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + name.hashCode();
        result = prime * result + surname.hashCode();
        result = prime * result + year;
        result = prime * result + id;
        result = prime * result + schedule.hashCode();
        result = prime * result + family.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Human other = (Human) obj;
        if (name != other.name)
            return false;
        if (surname != other.surname)
            return false;
        if (year != other.year)
            return false;
        if (id != other.id)
            return false;
        if (schedule != other.schedule)
                return false;
        if (family != other.family)
            return false;
        return true;
    }

    @Override
    protected void finalize() throws Throwable {
        if (family != null) {
            System.out.println("удален обьект типа Human: name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", year=" + year +
                    ", id=" + id +  "schedule" +schedule.toString());
        } else System.out.println("удален обьект типа Human: name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", id=" + id + " schedule= " +schedule.toString());
        super.finalize();
    }
}

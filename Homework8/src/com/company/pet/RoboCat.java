package com.company.pet;

import java.util.HashSet;

public class RoboCat  extends Pet implements Foul {
    private Species species;
    public RoboCat () {
        super();
    }
    public RoboCat (String nickname) {
        super(nickname);
    }
    public RoboCat (String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    {

    }

    @Override
    public void foul() {
        System.out.println("резистор выпал?");
    }

    @Override
    public void respond() {
        System.out.println("бип боп 11100100101001...");
    }
}

package com.company.pet;

public enum Species {
    Fish,
    DomesticCat,
    Dog,
    RoboCat,
    UNKNOWN
}

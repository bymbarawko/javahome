package com.company.pet;

import java.util.HashSet;

public class DomesticCat extends Pet implements Foul {
    private Species species;
    public DomesticCat () {
        super();
    }
    public DomesticCat (String nickname) {
        super(nickname);
    }
    public DomesticCat (String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    {

    }

    @Override
    public void foul() {
        System.out.println("Нужно сменить наполнитель...");
    }

    @Override
    public void respond() {
        System.out.println("мрррррр..ррмяу!");
    }
}

package com.company.pet;

import java.util.Arrays;
import java.util.HashSet;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private HashSet<String> habits = new HashSet<String>();

    static {
        System.out.println("загружаеться класс Pet");
    }
    {
        System.out.println("создан новый обьект класса: "+this.getClass().getSimpleName());
        this.species = Species.valueOf(this.getClass().getSimpleName());
    }

    public Pet(){

    }
    public Pet(String nickname) {
        this.nickname = nickname;
    }
    public Pet(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public HashSet<String> getHabits() {
        return habits;
    }

    //getters
    public Species getSpecies() {
        return this.species;
    }
    public String getNickname() {
        return this.nickname;
    }
    public int getAge() {
        return this.age;
    }
    public int getTrickLevel() {
        return this.trickLevel;
    }
    //setters(зачем?)
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(HashSet<String> habits) {
        this.habits = habits;
    }

    void eat() {
        System.out.println("Я кушаю!");
    }
    public abstract void respond();

//    void foul() {
//        System.out.println("Нужно хорошо замести следы...");
//    }

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + species.hashCode();
        if(nickname != null) result = prime * result + nickname.hashCode();
        result = prime * result + age;
        result = prime * result + trickLevel;
        result = prime * result + Arrays.hashCode(habits.toArray());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pet other = (Pet) obj;
        if (species != other.species)
            return false;
        if (nickname != other.nickname)
            return false;
        if (age != other.age)
            return false;
        if (trickLevel != other.trickLevel)
            return false;
            if (habits.toArray() != other.habits.toArray())
                return false;

        return true;
    }

    @Override
    public String toString() {
        return species+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits.toArray()) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("удален обьект типа Pet: species " + species+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits.toArray()) +
                '}');
        super.finalize();
    }
}


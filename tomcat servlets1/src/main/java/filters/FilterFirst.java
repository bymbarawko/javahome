package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(filterName = "FilterFirst", urlPatterns = "/lol")
public class FilterFirst implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
//        System.out.println("mi bili v filtre!");
//        resp.getWriter().println("mi bili v filtre!");
//        if(request.getParameter("name").equals("lol")){
//            resp.getWriter().println("vi nfpisali 'lol'!");
//            System.out.println("vi nfpisali 'lol'!");
//        } else {
            chain.doFilter(req, resp);
//        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

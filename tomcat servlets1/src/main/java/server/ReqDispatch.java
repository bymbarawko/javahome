package server;

import base.Base;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(name = "ReqDispatch", urlPatterns = "/lol")
public class ReqDispatch extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("POST!");


//        String lol = "lol";
//        req.setAttribute("lol", lol);

//        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/B");
//        dispatcher.forward(req, resp);

//        System.out.println("posle dispрatchera!");

        req.setCharacterEncoding("UTF-8");

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String reqData = req.getReader()
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
        Base.getData().add(reqData);
        out.print(reqData);
        out.flush();
        System.out.println("реквест: "+reqData);
        System.out.println("konec raboti servleta");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("GET!");

//        конфигурируем темплейт
        Configuration cfg = new Configuration();
        cfg.setIncompatibleImprovements(new Version("2.3.23"));
        cfg.setClassForTemplateLoading(ReqDispatch.class, "/");
        cfg.setDefaultEncoding("UTF-8");
        Template template = cfg.getTemplate("base.ftl");
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("data", Base.getData());

//        работа темплейта
//        try(StringWriter out = new StringWriter()){
//            template.process(templateData, out);
//            System.out.println(out.getBuffer().toString());
//            out.flush();
//        } catch (TemplateException e) {
//            e.printStackTrace();
//        }

//        пытаемся создать файл с темплейта
        try(Writer out = new FileWriter(new File("D:\\git\\javahome\\tomcat servlets1\\data.html"))){
            template.process(templateData, out);
            System.out.println("sozdalsia file");
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }



        System.out.println(Base.getData());
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(Base.getData());
        resp.getWriter().print("вот");
        System.out.println("uwlo");
    }
}

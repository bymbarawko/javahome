package com.company;

import java.util.Scanner;

public class input {
    public static int input(int[][] field) {
        Scanner in = new Scanner(System.in);
        String playerline = in.nextLine();

        if (isInt.doneInt(playerline) > 0 & isInt.doneInt(playerline) < field.length+1) {
            return isInt.doneInt(playerline);
        }

        return 0;
    }
}

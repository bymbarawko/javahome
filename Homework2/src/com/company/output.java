package com.company;

public class output {
    public static void outputField(int[][] field) {
        for (int i = 0; i < field.length; i++) {
            if(i == 0) {
                for (int k = 0; k < field[i].length+1; k++) {
                    System.out.print(k+" "+"|"+" ");
                }
                System.out.println();
            }
            for (int j = 0; j < field[i].length; j++) {
                if (j == 0) {
                    System.out.print(i+1+" "+"|"+" ");
                }
                if (field[i][j] == 2) {
                    System.out.print("*"+" "+"|"+" ");
                }
                if (field[i][j] == 3) {
                    System.out.print("x"+" "+"|"+" ");
                }
                if (field[i][j] == 0) {
                    System.out.print("-"+" "+"|"+" ");
                }
                if (field[i][j] == 1) {
                    System.out.print("-"+" "+"|"+" ");
                }
            }
            System.out.println();
        }
    }
}

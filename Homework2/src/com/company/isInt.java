package com.company;

public class isInt {
    public static boolean isNumeric(String strNum) {
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public static int doneInt (String playerIn) {
        if (isNumeric(playerIn)) {
            return Integer.parseInt(playerIn);
        }
        return 0;
    }
}

package com.company.pet;

import java.util.HashSet;

public class Dog extends Pet implements FoulingPet {
    private String nickname;
    private Species species;
    public Dog () {
        super();
    }
    public Dog (String nickname) {
        super(nickname);
    }
    public Dog (String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    {

    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - "+nickname+". Я соскучился!");
    }
}

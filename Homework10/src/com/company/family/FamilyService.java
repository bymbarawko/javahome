package com.company.family;

import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;
import com.company.pet.Pet;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class FamilyService {
    CollectionFamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        for(Family family : familyDao.getAllFamilies()){
            System.out.print(family.toString()+"  ░▒▓▒░  ");
        }
        System.out.println();
    }


    public void getFamiliesBiggerThan(int count) {
        for (Family family : familyDao.getAllFamilies()) {
            if (count < family.countFamily()) {
                System.out.print(family.toString()+"  ░▒▓▒░  ");
            }
        }
        System.out.println();
    }


    public void getFamiliesLessThan(int count) {
        for(Family family : familyDao.getAllFamilies()){
            if(count > family.countFamily()){
                System.out.print(family.toString()+"  ░▒▓▒░  ");
            }
        }
        System.out.println();
    }


    public int countFamiliesWithMemberNumber(int count) {
        int countf = 0;
        for(Family family : familyDao.getAllFamilies()){
            if(count == family.countFamily()){
                countf++;
            }
        }
        return countf;
    }


    public void createNewFamily(Human parent1, Human parent2) {
        Family family = new Family(parent1, parent2);
        familyDao.saveFamily(family);
    }


    public Family getFamilyById (int index) {
        return familyDao.getFamilyByIndex (index);
    }


    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamily(index);
    }

    //оставил чтоб было)
    public void deleteFamilyByFamily(Family family) {
        familyDao.deleteFamily(family);
    }


    public Family bornChild(Family family, String nameM, String nameW) {
        Human child;
        Random random = new Random();
        if(random.nextInt(2) == 1){
            child = new Man();
            child.setName(nameM);
        }
        else {
            child = new Woman();
            child.setName(nameW);
        }
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }


    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }


    public void deleteAllChildrenOlderThen(int year) {
        for(Family family : familyDao.getAllFamilies()){
            family.getChildren().removeIf(human -> human.getBirthDate() > year);
            familyDao.saveFamily(family);
        }
    }


    public int count() {
        return familyDao.getAllFamilies().size();
    }



    public HashSet<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPet();
    }


    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.addPet(pet);
        saveFamily(family);

    }





//for test)
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }


}

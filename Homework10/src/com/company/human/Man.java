package com.company.human;

import com.company.pet.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

public final class Man extends Human {
    private long birthDate;

    public Man(){
        super();
    }
    public Man(String name, String surname, long birthDate, int id){
        super(name, surname, birthDate, id);
    }
    public Man(String name, String surname, String birthDate, int id){
        super(name, surname, birthDate, id);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date data = null;
        try {
            data = format.parse(birthDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert data != null;
        this.birthDate = data.getTime();
    }
    public Man(String name, String surname, long birthDate, int id, HashMap<String, String> schedule){
        super(name, surname, birthDate, id, schedule);
    }

    {

    }

    @Override
    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) System.out.println("Даров епта, " + pet.getNickname());
            }
        }
    }
    @Override
    public void repairCar() {
        System.out.println("чинит машину...");
    }

    @Override
    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
}

package com.company;

import java.util.Arrays;

public class Main {

    static {
        System.out.println("загружаеться класс Main");
    }

    {
        System.out.println("Main: новый экземпляр");
    }

    public static void main(String[] args) {
        Pet dog = new Pet("dog", "хороший мальчик", 10, 60, new String[] {"sleep", "eat"});
        Pet dog1 = new Pet("dog", "хороший мальчик", 10, 60, new String[] {"sleep", "eat"});
        Pet cat = new Pet("cat", "murzik");
        Pet beast = new Pet();

        Human bitard = new Human("nagibator228", "ivanov", 15, 1);
        Human vasia = new Human("ivan", "ivanov", 20, 80);

        Human mother = new Human();
        Human father = new Human();
        Human sasha = new Human("Sasha", "lenivii", 18, 100,new String[][] {{"пн", "ср"},{"пожрать","поспать"}});

        Family smits = new Family(mother, sasha);
        mother.setFamily(smits);
        System.out.println(smits.countFamily());
        smits.addChild(new Human("1", "1", 1, 1));
        smits.addChild(vasia);
        System.out.println(Arrays.toString(smits.getChildren()));
        smits.addChild(new Human("2", "2", 2, 2));
        smits.addChild(new Human("3", "3", 3, 3));
        smits.addChild(new Human("4", "4", 4, 4));
        smits.addChild(new Human("5", "5", 5, 5));
        smits.addChild(new Human("6", "6", 6, 6));
        System.out.println(smits.countFamily());
        boolean lol = smits.deleteChild(2);
        System.out.println(smits.countFamily());
        smits.deleteChildByLink(vasia);
        System.out.println(Arrays.toString(smits.getChildren()));


//        bitard.greetPet();
//        bitard.describePet();
//        System.out.println(bitard.toString());
//
//        vasia.greetPet();
//        vasia.describePet();
//        System.out.println(vasia.toString());
//
//        sasha.greetPet();
//        sasha.describePet();
//        System.out.println(sasha.toString());
//
//        mother.greetPet();
//        mother.describePet();
//        System.out.println(mother.toString());
//
//        dog.eat();
//        dog.respond();
//        dog.foul();
//        System.out.println(dog.toString());
//
//        cat.eat();
//        cat.respond();
//        cat.foul();
//        System.out.println(cat.toString());
//
//        beast.eat();
//        beast.respond();
//        beast.foul();
//        System.out.println(beast.toString());
    }
}

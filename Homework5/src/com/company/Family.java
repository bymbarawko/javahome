package com.company;

import java.util.Arrays;
import java.util.List;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    static {
        System.out.println("загружаеться класс Family");
    }

    {
        System.out.println("Family: новый экземпляр");
    }

    Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    //getters
    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public Human[] getChildren() {
        return this.children;
    }

    public Pet getPet() {
        return this.pet;
    }

    //setters(ну я же их не вызову, неа)
    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        if (children == null) {
            this.children = new Human[]{child};
        } else {
            this.children = Arrays.copyOf(this.children, this.children.length + 1);
            children[children.length - 1] = child;
            children[children.length - 1].setFamily(this);
        }
    }

    public boolean deleteChild(int index) {
        boolean deletey = false;
        if(children != null ) {
            for (int i = 0; i < children.length; i++) {
                if (i == index) {
                    children[i].setFamily(null);
                    for (int j = i; j < children.length-1; j++) {
                        this.children[j] = children[j + 1];
                    }
                    if (children[i] != null)
                        children[i].setFamily(null);
                    deletey = true;
                }
            }
            if(deletey == true)
                this.children = Arrays.copyOf(this.children, this.children.length - 1);
        }

        return deletey;
    }

    public void deleteChildByLink(Human deleted) {
        for (int i = 0; i < children.length; i++) {
            if (deleted.equals(children[i])) {
                for (int j = i; j < children.length-1; j++) {
                    this.children[j] = children[j + 1];
                    System.out.println("DELETED "+j);
                }
                if (children[i] != null)
                    children[i].setFamily(null);
            }
        }
        this.children = Arrays.copyOf(this.children, this.children.length - 1);
    }

    public int countFamily() {
        if(children != null) {
            if(children.length > 0) {
                return children.length + 2;
            }
        }
        return 2;
    }

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + mother.hashCode();
        result = prime * result + father.hashCode();
        result = prime * result + Arrays.hashCode(children);
        result = prime * result + pet.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Family other = (Family) obj;
        if (mother != other.mother)
            return false;
        if (father != other.father)
            return false;
        for (int o = 0; o > children.length; o++) {
            if (children[o] != other.children[o])
                return false;
        }
        if (pet != other.pet)
            return false;
        return true;
    }
}

package com.company;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("загружаеться класс Pet");
    }

    {
        System.out.println("Pet: новый экземпляр");
    }

    Pet(){

    }
    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    //getters
    public String getSpecies() {
        return this.species;
    }
    public String getNickname() {
        return this.nickname;
    }
    public int getAge() {
        return this.age;
    }
    public int getTrickLevel() {
        return this.trickLevel;
    }
    //setters(зачем?)
    public void setSpecies(String species) {
        this.species = species;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    void eat() {
        System.out.println("Я кушаю!");
    }
    void respond() {
        System.out.println("Привет, хозяин. Я - "+nickname+". Я соскучился!");
    }
    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + species.hashCode();
        result = prime * result + nickname.hashCode();
        result = prime * result + age;
        result = prime * result + trickLevel;
        result = prime * result + Arrays.hashCode(habits);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pet other = (Pet) obj;
        if (species != other.species)
            return false;
        if (nickname != other.nickname)
            return false;
        if (age != other.age)
            return false;
        if (trickLevel != other.trickLevel)
            return false;
        for(int o = 0; o > habits.length; o++) {
            if (habits[o] != other.habits[o])
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return species+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}

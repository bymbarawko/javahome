<!DOCTYPE html>
<html>
<head>
    <title>Start Page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        body {
            font-size: 120%;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            color: #333366;
        }
        .forma {
            display: inline-block;
            margin: 0;
            margin-bottom: 5px;
        }
        .res {
            margin: -4px;
        }
    </style>
</head>
<body>
<form action="http://localhost:8080/servletDispatch/lol" method="POST">
    <p class="forma">
        ${msg}
    </p>
    <p  class="forma">
        <input name="name" placeholder="name" class="name">
    </p>
    <div  class="forma"><input type="submit" class="post"></div>
    <p id="output"  class="forma"></p>
</form>

<div class="lol">
<#list data as String>
    <div class="res" style="border: cyan 3px solid; border-radius: 15px; display: inline-block; padding: 15px; background-color: aquamarine">json object: ${String}</div>
</#list>
</div>

<script>

    document.querySelector(".post").addEventListener("click", async function () {
        event.preventDefault();
        const name = document.querySelector('.name').value;
        const data = {
            name: name,
        };
        console.log("отправляю: " + JSON.stringify(data));
        const response = await fetch("http://localhost:8090/status", {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            // mode: 'cors', // no-cors, cors, *same-origin
            // cache: 'force-cache', // *default, no-cache, reload, force-cache, only-if-cached
            // credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            // redirect: 'follow', // manual, *follow, error
            // referrer: 'no-referrer' // no-referrer, *client
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        }).catch(e => {console.log('Bad URL: ', e)});
        let res = await response.json();
        location.reload();

        console.log(res);
    });

</script>

</body>
</html>
package myserver.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import myserver.base.Base;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MyServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Post запрос!");
        req.setCharacterEncoding("UTF-8");

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        String reqData = req.getReader()
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
        Base.getData().add(reqData);
        out.print(reqData);
        out.flush();


        System.out.println("Post запрос выполнило!");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Get запрос!");
//        конфигурируем темплейт
        Configuration cfg = new Configuration();
        cfg.setIncompatibleImprovements(new Version("2.3.23"));
        cfg.setClassForTemplateLoading(MyServlet.class, "/");
        cfg.setDefaultEncoding("UTF-8");
        Template template = cfg.getTemplate("base.ftl");
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("data", Base.getData());
        templateData.put("msg", "Post");

        //        работа темплейта с выводом результата в консоль
//        try(StringWriter out = new StringWriter()){
//            template.process(templateData, out);
//            System.out.println(out.getBuffer().toString());
//            out.flush();
//        } catch (TemplateException e) {
//            e.printStackTrace();
//        }

//        пытаемся создать файл с темплейта
//        try(Writer out = new FileWriter(new File("D:\\git\\javahome\\jettyserver\\data.html"))){
//            template.process(templateData, out);
//            System.out.println("sozdalsia file");
//        } catch (Exception ex){
//            throw new RuntimeException(ex);
//        }

//        try(Reader in = new FileReader(new File("D:\\git\\javahome\\jettyserver\\data.html"))){
//            resp.getWriter().println(in);
//        }
//        resp.setContentType("application/json");
//        resp.setCharacterEncoding("UTF-8");

        try(Writer out = resp.getWriter()){
            template.process(templateData, out);
        } catch (TemplateException e) {
            e.printStackTrace();
        }
        System.out.println("Get запрос выполнило!");
    }
}

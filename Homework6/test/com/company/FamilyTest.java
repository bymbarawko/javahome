package com.company;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyTest {


    @Test
    public void testDeleteChild() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        Human lol3 = new Human("3", "3", 3, 3);
        family.addChild(lol1);
        family.addChild(lol2);
        family.addChild(lol3);
        List<Human> expected = Arrays.asList(family.getChildren());

        Human[] n = family.getChildren();
        boolean expectedF = family.deleteChild(10);
        List<Human> actual = Arrays.asList(family.getChildren());

        //если индекс неверный:
        assertEquals(expected, actual);
        assertFalse(expectedF);

        //если все ок и нужный обьект должен быть удален:
        boolean expectedy = family.deleteChild(1);
        assertTrue(expectedy);
        assertEquals(n[1], lol3);
    }


    @Test
    public void testAddChild() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        family.addChild(lol1);

        List<Human> expected = Arrays.asList(family.getChildren());

        List<Human> actual = new ArrayList<>();
        actual.add(lol1);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testCountFamily() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        family.addChild(lol1);
        family.addChild(lol2);

        int expected = family.countFamily();

        Assert.assertEquals(expected, 4);
    }

    @Test
    public void testToString() {
        Family family = new Family(new Human(), new Human());
        String expected = family.toString();

        String actual = "name='" + family.getMother() + '\'' +
                ", surname='" + family.getFather() + '\'' +
                ", year=" + Arrays.toString(family.getChildren()) +
                ", id=" + family.getPet();

        Assert.assertEquals(expected, actual);
    }
}
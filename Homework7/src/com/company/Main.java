package com.company;


import com.company.SpeciesPet.Pet;
import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;

public class Main {

    static {
        System.out.println("загружаеться класс Main");
    }

    {
        System.out.println("Main: новый экземпляр");
    }


    public static void main(String[] args) {
        Pet dog = new com.company.SpeciesPet.Dog("хороший мальчик", 10, 60, new String[]{"sleep", "eat"});
        Pet dog1 = new com.company.SpeciesPet.Fish("немо", 10, 60, new String[]{"swim", "eat"});
        Pet cat = new com.company.SpeciesPet.DomesticCat("murzik");
        Pet beast = new com.company.SpeciesPet.RoboCat();

        String[][] raspisanie = new String[7][2];
        raspisanie[0][0] = DayOfWeek.MONDAY.name();
        raspisanie[1][0] = DayOfWeek.TUESDAY.name();
        raspisanie[2][0] = DayOfWeek.WEDNESDAY.name();
        raspisanie[3][0] = DayOfWeek.THURSDAY.name();
        raspisanie[4][0] = DayOfWeek.FRIDAY.name();
        raspisanie[5][0] = DayOfWeek.SATURDAY.name();
        raspisanie[6][0] = DayOfWeek.SUNDAY.name();


        Human bitard = new com.company.human.Human("nagibator228", "ivanov", 15, 1);
        Human vasia = new com.company.human.Human("ivan", "ivanov", 20, 80, raspisanie);

        Woman mother = new com.company.human.Woman("Vasilisa", "levchenko", 23, 70, new String[][]{{"пн", "ср"}, {"шоппинг", "сериалы"}});
        Man father = new com.company.human.Man("Sasha", "levchenko", 25, 90, new String[][]{{"пн", "ср"}, {"пожрать", "поспать"}});

        mother.makeup();
        father.repairCar();
        Family smits = new Family(mother, father);
        Human child = smits.bornChild();
        System.out.println(child);
//        smits.deleteChild(1);
//        mother.setFamily(smits);
//        System.out.println(smits.countFamily());
//        smits.addChild(new Human("1", "1", 1, 1));
//        System.out.println(Arrays.toString(smits.getChildren()));
//
//
//        smits.addChild(new Human("2", "2", 2, 2));
//        smits.addChild(new Human("3", "3", 3, 3));
//        smits.addChild(new Human("4", "4", 4, 4));
//        smits.addChild(new Human("5", "5", 5, 5));
//        smits.addChild(new Human("6", "6", 6, 6));


//        smits.addChild(vasia);
//        smits.addChild(new Human("4", "4", 4, 4));
//        System.out.println(smits.countFamily());
//        smits.deleteChildByLink(vasia);
//        boolean lol = smits.deleteChild(0);
//        smits.deleteChild(0);
//        System.out.println(smits.countFamily());
//        System.out.println(Arrays.toString(smits.getChildren()));
//        int intoviia = 0;
//        while (true) {
//            String stringoviia = String.valueOf(intoviia);
//            smits.addChild(new Human(stringoviia, stringoviia, intoviia, intoviia));
//            System.out.println("номер итерации " + intoviia);
//            lol = smits.deleteChild(intoviia);
//            System.out.println("численность семьи: " + smits.countFamily());
//            intoviia++;
//        }


//        bitard.greetPet();
//        bitard.describePet();
//        System.out.println(bitard.toString());
//
//        vasia.greetPet();
//        vasia.describePet();
//        System.out.println(vasia.toString());
//
//        sasha.greetPet();
//        sasha.describePet();
//        System.out.println(sasha.toString());
//
//        mother.greetPet();
//        mother.describePet();
//        System.out.println(mother.toString());
//
//        dog.eat();
//        dog.respond();
//        dog.foul();
//        System.out.println(dog.toString());
//
//        cat.eat();
//        cat.respond();
//        cat.foul();
//        System.out.println(cat.toString());
//
//        beast.eat();
//        beast.respond();
//        beast.foul();
//        System.out.println(beast.toString());
    }
}

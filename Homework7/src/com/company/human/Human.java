package com.company.human;


import com.company.Family;

import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int id;
    private String[][] schedule;
    protected static Family family;

    static {
        System.out.println("загружаеться класс Human");
    }

    {
//        System.out.println("Human: новый экземпляр, я хз как это не юзать если создаеться вложенный класс, лол");
    }

    public Human() {

    }
    public Human(String name, String surname, int year, int id) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
    }
    public Human(String name, String surname, int year, int id, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
        this.schedule = schedule;
    }


    //getters
    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public int getYear() {
        return this.year;
    }
    public int getId() {
        return this.id;
    }
    public String[][] getSchedule() {
        return this.schedule;
    }
    public Family getFamily() {
        return this.family;
    }

    //setters(зачеееееем?))
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    public void setFamily(Family family) {
        this.family = family;
    }


    void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                if (family.getPet().getNickname() != null) {
                    System.out.println("Привет, "+family.getPet().getNickname());
                }
            }
        }
        else {
            System.out.println("Привет, одиночество))");
        }
    }
    void describePet() {

        if (family != null) {
            if (family.getPet() != null) {
                    if (family.getPet().getTrickLevel() > 50) {
                        System.out.println("У меня есть "+family.getPet().getSpecies()+", ему "+family.getPet().getAge()+" лет, он очень хитрый");
                    }
                    else {
                        System.out.println("У меня есть "+family.getPet().getSpecies()+", ему "+family.getPet().getAge()+" лет, он почти не хитрый");
                    }
            }
        }

        else {
            System.out.println("У меня нет питомца.");
        }
    }
    void makeup() {

    }
    void repairCar() {

    }




    @Override
    public String toString() {
            return  "Пол= "+this.getClass().getSimpleName()+" "+"name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", year=" + year +
                    ", id=" + id +  "schedule" +Arrays.toString(schedule);
    }



    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + name.hashCode();
        result = prime * result + surname.hashCode();
        result = prime * result + year;
        result = prime * result + id;
        result = prime * result + Arrays.hashCode(schedule);
        result = prime * result + family.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Human other = (Human) obj;
        if (name != other.name)
            return false;
        if (surname != other.surname)
            return false;
        if (year != other.year)
            return false;
        if (id != other.id)
            return false;
        for(int o = 0; o > schedule.length; o++) {
            if (schedule[o][o] != other.schedule[o][o])
                return false;
        }
        if (family != other.family)
            return false;
        return true;
    }

    @Override
    protected void finalize() throws Throwable {
        if (family != null) {
            System.out.println("удален обьект типа Human: name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", year=" + year +
                    ", id=" + id +  "schedule" +Arrays.toString(schedule));
        } else System.out.println("удален обьект типа Human: name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", id=" + id + " schedule= " +Arrays.toString(schedule));
        super.finalize();
    }
}

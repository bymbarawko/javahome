package com.company.human;

import java.util.Arrays;

public class Man extends Human {
    public Man(){
        super();
    }
    public Man(String name, String surname, int year, int id){
        super(name, surname, year, id);
    }
    public Man(String name, String surname, int year, int id, String[][] schedule){
        super(name, surname, year, id, schedule);
    }

    {
        System.out.println("Human: новый экземпляр: Man");
    }

    @Override
    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                if (family.getPet().getNickname() != null) {
                    System.out.println("Даров епта, "+family.getPet().getNickname());
                }
            }
        }
    }
    @Override
    public void repairCar() {
        System.out.println("чинит машину...");
    }
}

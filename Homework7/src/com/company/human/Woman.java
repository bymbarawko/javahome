package com.company.human;

public class Woman extends Human {
    public Woman(){
        super();
    }
    public Woman(String name, String surname, int year, int id){
        super(name, surname, year, id);
    }
    public Woman(String name, String surname, int year, int id, String[][] schedule){
        super(name, surname, year, id, schedule);
    }

    {
        System.out.println("Human: новый экземпляр: Woman");
    }

    @Override
    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                if (family.getPet().getNickname() != null) {
                    System.out.println("Сейчас мы тебя накрасим, "+family.getPet().getNickname());
                }
            }
        }
    }
    @Override
    public void makeup() {
        System.out.println("*звуки красящийся женщины*");
    }
}

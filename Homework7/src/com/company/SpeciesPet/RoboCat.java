package com.company.SpeciesPet;

public class RoboCat  extends Pet {
    private com.company.Species species;
    public RoboCat () {
        super();
    }
    public RoboCat (String nickname) {
        super(nickname);
    }
    public RoboCat (String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    {
        this.species = com.company.Species.RoboCat;
        System.out.println("Pet: новый экземпляр: RoboCat");
    }

    @Override
    public void respond() {
        System.out.println("бип боп 11100100101001...");
    }
}

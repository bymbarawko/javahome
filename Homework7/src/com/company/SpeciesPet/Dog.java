package com.company.SpeciesPet;

public class Dog extends Pet {
    private String nickname;
    private com.company.Species species;
    public Dog () {
        super();
    }
    public Dog (String nickname) {
        super(nickname);
    }
    public Dog (String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    {
        this.species = com.company.Species.Dog;
        System.out.println("Pet: новый экземпляр: Dog");
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - "+nickname+". Я соскучился!");
    }
}

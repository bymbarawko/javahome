package com.company.SpeciesPet;

public class Fish extends Pet {
    private com.company.Species species;
    public Fish () {
        super();
    }
    public Fish (String nickname) {
        super(nickname);
    }
    public Fish (String nickname, int age, int trickLevel, String[] habits) { super(nickname, age, trickLevel, habits);
    }

    {
        this.species = com.company.Species.Fish;
        System.out.println("Pet: новый экземпляр: Fish");
    }

    @Override
    public void respond() {
        System.out.println("буль буль хлюп буль");
    }
}

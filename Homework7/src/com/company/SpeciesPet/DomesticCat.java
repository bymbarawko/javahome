package com.company.SpeciesPet;

public class DomesticCat extends Pet {
    private com.company.Species species;
    public DomesticCat () {
        super();
    }
    public DomesticCat (String nickname) {
        super(nickname);
    }
    public DomesticCat (String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    {
        this.species = com.company.Species.DomesticCat;
        System.out.println("Pet: новый экземпляр: DomesticCat");
    }

    @Override
    public void respond() {
        System.out.println("мрррррр..ррмяу!");
    }
}

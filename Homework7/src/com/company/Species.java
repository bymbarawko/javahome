package com.company;

public enum Species {
    Fish,
    DomesticCat,
    Dog,
    RoboCat,
    UNKNOWN
}

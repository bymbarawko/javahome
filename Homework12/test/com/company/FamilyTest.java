package com.company;

import com.company.family.Family;
import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyTest {


    @Test
    public void deleteChildWrightIndexReturnsTrue() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Man("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        Human lol3 = new Human("3", "3", 3, 3);
        family.addChild(lol1);
        family.addChild(lol2);
        family.addChild(lol3);
        List<Human> expected = family.getChildren();

        List<Human> n = family.getChildren();

        //если все ок и нужный обьект должен быть удален:
        boolean expectedy = family.deleteChild(1);
        assertTrue(expectedy);
        assertEquals(n.get(1), lol3);
    }

    @Test
    public void deleteChildWrongIndexReturnsFalse() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        Human lol3 = new Human("3", "3", 3, 3);
        family.addChild(lol1);
        family.addChild(lol2);
        family.addChild(lol3);
        List<Human> expected = family.getChildren();

        boolean expectedF = family.deleteChild(10);
        List<Human> actual = family.getChildren();

        //если индекс неверный:
        assertEquals(expected, actual);
        assertFalse(expectedF);
    }

    @Test
    public void deleteChildByLinkDone() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        Human lol3 = new Human("3", "3", 3, 3);
        family.addChild(lol1);
        family.addChild(lol2);
        family.addChild(lol3);

        List<Human> expected = family.getChildren();
        family.deleteChildByLink(lol1);

        List<Human> actual = family.getChildren();
        assertEquals(expected.get(1), lol3);
    }

    @Test
    public void deleteChildByLinkWrongObject() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        Human lol3 = new Human("3", "3", 3, 3);
        family.addChild(lol1);
        family.addChild(lol2);
        family.addChild(lol3);

        List<Human> expected = family.getChildren();
        family.deleteChildByLink(new Human());

        List<Human> actual = family.getChildren();
        assertEquals(expected.get(1), lol2);
    }


    @Test
    public void testAddChild() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        family.addChild(lol1);

        List<Human> expected = family.getChildren();

        List<Human> actual = new ArrayList<>();
        actual.add(lol1);

        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testCountFamily() {
        Family family = new Family(new Human(), new Human());
        Human lol1 = new Human("1", "1", 1, 1);
        Human lol2 = new Human("2", "2", 2, 2);
        family.addChild(lol1);
        family.addChild(lol2);

        int expected = family.countFamily();

        Assert.assertEquals(expected, 4);
    }

    @Test
    public void testToString() {
        Family family = new Family(new Human(), new Human());
        String expected = family.toString();

        String actual = "mother: " + family.getMother() + '\'' +
                ", father: " + family.getFather() + '\'' +
                ", childrens: " + family.getChildren() +
                ", pets: " + family.getPet();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testBornChildReturn() {
        Family family = new Family(new Man(), new Woman());
        Human child = family.bornChild();
        assertNotNull(child);
    }

    @Test
    public void testBornChildAddToChildArr() {
        Family family = new Family(new Man(), new Woman());
        Human child = family.bornChild();

        assertNotNull(family.getChildren());
    }

    @Test
    public void equals(){
        Woman mother = new com.company.human.Woman("Vasilisa", "levchenko", 23, 90);
        Man father = new com.company.human.Man("Sasha", "levchenko", 25, 90);

        Woman mother1 = new com.company.human.Woman("Vasilisa", "levchenko", 23, 70);
        Man father1 = new com.company.human.Man("Sasha", "levchenko", 25, 90);

        Family smist = new Family(mother, father);
        Family smist1 = new Family(mother1, father1);
        assertNotEquals(smist, smist1);
        assertEquals(smist, smist);
    }

}
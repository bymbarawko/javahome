package com.company.human;


import com.company.family.Family;
import com.company.pet.Pet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.Temporal;
import java.util.*;

import static java.time.temporal.ChronoUnit.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int id;
    private HashMap<String, String> schedule;
    private Family family;

    static {
        System.out.println("загружаеться класс Human");
    }

    {
        System.out.println("создан новый обьект класса: "+this.getClass().getSimpleName());
    }

    public Human() {

    }
    public Human(String name, String surname, long birthDate, int id) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.id = id;
    }
    public Human(String name, String surname, String birthDate, int id) {
        this.name = name;
        this.surname = surname;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date data = null;
        try {
            data = format.parse(birthDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert data != null;
        this.birthDate = data.getTime();
        this.id = id;
    }
    public Human(String name, String surname, long birthDate, int id, HashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.id = id;
        this.schedule = schedule;
    }


    //getters
    public String getName() {
        return this.name;
    }
    public String getSurname() {
        return this.surname;
    }
    public long getBirthDate() {
        return this.birthDate;
    }
    public int getId() {
        return this.id;
    }
    public HashMap<String, String> getSchedule() {
        return this.schedule;
    }
    public Family getFamily() {
        return this.family;
    }

    //setters(зачеееееем?))
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }
    public void setFamily(Family family) {
        this.family = family;
    }


    public void greetPet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) System.out.println("Привет, " + pet.getNickname());
            }
        }
        else {
            System.out.println("Привет, одиночество))");
        }
    }
    public void describePet() {
        if (family != null) {
            if (family.getPet() != null) {
                for (Pet pet : family.getPet()) {
                    if (pet.getTrickLevel() > 50) {
                        System.out.println("У меня есть "+pet.getSpecies()+", ему "+pet.getAge()+" лет, он очень хитрый");
                    }
                    else {
                        System.out.println("У меня есть "+pet.getSpecies()+", ему "+pet.getAge()+" лет, он почти не хитрый");
                    }
                }
            }
        }

        else {
            System.out.println("У меня нет питомца.");
        }
    }
    void makeup() {

    }
    void repairCar() {

    }

    public String describeAgeUPRT() {
        //не знаю на сколько это упорото но я сделяль)
        DateFormat days = new SimpleDateFormat("dd");
        DateFormat months = new SimpleDateFormat("MM");
        DateFormat years = new SimpleDateFormat("yyyy");

        BigInteger letEdak1970InMsPoVersiiJava = new BigInteger("62135780400000");
        BigInteger year1970LolForParser = new BigInteger("0");
        BigInteger year0LolForParser = new BigInteger(String.valueOf(year1970LolForParser.subtract(letEdak1970InMsPoVersiiJava)));
        long age = System.currentTimeMillis() - this.birthDate;

        BigInteger ageForParser = new BigInteger(String.valueOf(year0LolForParser.add(BigInteger.valueOf(age))));

        int godaint = Integer.parseInt(years.format(ageForParser)) - 1;
        int monthsint = Integer.parseInt(months.format(ageForParser)) - 1;
        int daysint = Integer.parseInt(days.format(ageForParser)) - 1;

        if(this.getClass().getSimpleName().equals("Woman")){
            return "ей: "+ godaint +" лет, "+monthsint+" месяцев, "+daysint+" дней)";
        }
        return "ему: "+ godaint +" лет, "+monthsint+" месяцев, "+daysint+" дней)";
    }

    public String describeAge(){
        LocalDate birthDateTime = new Timestamp(birthDate).toLocalDateTime().toLocalDate();
        LocalDate now = LocalDate.now();
        Period difference = Period.between(birthDateTime, now);
        return "ему: "+difference.getYears()+" лет, "+difference.getMonths()+" месяцев, "+difference.getDays()+" дней";
    }

    public void prettyFormat() {
        System.out.println("Human: ");
        System.out.println("    " + "Sex: " + this.getClass().getSimpleName());
        System.out.println("    " + "Name: " + this.name);
        System.out.println("    " + "Surname: " + this.surname);
        System.out.println("    " + "Bird date: " + new Timestamp(birthDate).toLocalDateTime().toLocalDate());
        System.out.println("    " + "Iq: " + this.id);
        System.out.println("    " + "Schedule: ");
        if (this.schedule != null) this.schedule
                .forEach((e, v) -> System.out.println("    " + "    " + e +" " + v));
        this.family.prettyFormat();
    }


    @Override
    public String toString() {
        Calendar gc = new GregorianCalendar();
        gc.setTimeInMillis(birthDate);
            return  "Gender= "+this.getClass().getSimpleName()+" "+"name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", birthDate= " + gc.get(Calendar.DATE)+"/"+(gc.get(Calendar.MONTH)+1)+"/"+gc.get(Calendar.YEAR)+
                    ", id=" + id +  "schedule" +schedule;
    }



    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + name.hashCode();
        result = prime * result + surname.hashCode();
        result = (int) (prime * result + birthDate);
        result = prime * result + id;
        result = prime * result + schedule.hashCode();
        result = prime * result + family.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Human other = (Human) obj;
        if (name != other.name)
            return false;
        if (surname != other.surname)
            return false;
        if (birthDate != other.birthDate)
            return false;
        if (id != other.id)
            return false;
        if (schedule != other.schedule)
                return false;
        if (family != other.family)
            return false;
        return true;
    }

    @Override
    protected void finalize() throws Throwable {
        Calendar gc = new GregorianCalendar();
        gc.setTimeInMillis(birthDate);
        if (family != null) {
            System.out.println("удален обьект типа Human: name='" + name + '\'' +
                    ", surname= " + surname + '\'' +
                    ", birthDate= " + gc.get(Calendar.DATE)+"/"+gc.get(Calendar.MONTH)+"/"+gc.get(Calendar.YEAR)+
                    ", id= " + id +  "schedule" +schedule.toString());
        } else System.out.println("удален обьект типа Human: name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate= " + gc.get(Calendar.DATE)+"/"+gc.get(Calendar.MONTH)+"/"+gc.get(Calendar.YEAR)+
                ", id=" + id + " schedule= " +schedule.toString());
        super.finalize();
    }

    public void setBirthDate() {
    }
}

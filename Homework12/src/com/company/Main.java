package com.company;


import com.company.family.*;
import com.company.pet.Pet;
import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    static {
        System.out.println("загружаеться класс Main");
    }

    {
        System.out.println("Main: новый экземпляр");
    }


    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        HashSet<String> habits = new HashSet<>();
        habits.add("жрат");
        habits.add("спат");

        Date data = null;

        try {
            data = format.parse("12.02.1578");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assert data != null;
        long birddate = data.getTime();

        Pet dog = new com.company.pet.Dog("хороший мальчик", 10, 60, habits);
        Pet dog1 = new com.company.pet.Fish("немо", 10, 60, habits);
        Pet cat = new com.company.pet.DomesticCat("murzik");
        Pet beast = new com.company.pet.RoboCat();

        HashSet<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(dog1);
        pets.add(cat);

        HashMap<String, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY.name(), "прокрастинация");
        schedule.put(DayOfWeek.TUESDAY.name(), "прокрастинация");
        schedule.put(DayOfWeek.WEDNESDAY.name(), "прокрастинация");
        schedule.put(DayOfWeek.THURSDAY.name(), "прокрастинация");
        schedule.put(DayOfWeek.FRIDAY.name(), "прокрастинация");
        schedule.put(DayOfWeek.SATURDAY.name(), "прокрастинация");
        schedule.put(DayOfWeek.SUNDAY.name(), "прокрастинация");


        Human alisa = new com.company.human.Woman("alisa", "̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞", "11/11/2001", 95);
        Human softwareArchitectForJava = new com.company.human.Human("ivan", "̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞", birddate, 200);


        Woman mother = new com.company.human.Woman("Vasilisa", "levchenko", "20/11/1996", 70);
        Man father = new com.company.human.Man("Sasha", "levchenko", "11/02/1994", 90);
        Man man = new com.company.human.Man("Sasha", "levchenko", "20/11/1994", 90);

        FamilyController familyDB = new FamilyController();

        Family smist = new Family(mother, father);
        Family bestfamily = new Family(alisa, softwareArchitectForJava);

        familyDB.saveFamily(smist);
        familyDB.saveFamily(bestfamily);
//        System.out.println(familyDB.getAllFamilies());
//        familyDB.displayAllFamilies();
//        familyDB.bornChild(smist, "lesha", "alena");
//        familyDB.getFamiliesBiggerThan(2);
//        familyDB.getFamiliesLessThan(3);
//        familyDB.createNewFamily(new Human(), new Human());
//        System.out.println(familyDB.countFamiliesWithMemberNumber(2));
//        familyDB.deleteFamilyByIndex(1);
//        familyDB.adoptChild(smist, new Human());
//        familyDB.deleteAllChildrenOlderThen(1);
//        System.out.println(familyDB.count());
//        System.out.println(familyDB.getFamilyById(0));
//        familyDB.addPet(1, cat);
//        System.out.println(familyDB.getPets(1));

//        System.out.println(softwareArchitectForJava.toString());
//        System.out.println(softwareArchitectForJava.describeAge());
//        System.out.println(alisa.toString());
//        System.out.println(alisa.describeAge());
//        System.out.println(man.toString());
//        System.out.println(man.describeAge());

//        familyDB.displayAllFamilies();
//        System.out.println(familyDB.getFamiliesBiggerThan(1));
//        System.out.println(familyDB.countFamiliesWithMemberNumber(2));
//        familyDB.bornChild(smist, "lesha", "alena");
//        familyDB.bornChild(smist, "lesha", "alena");
//        familyDB.bornChild(smist, "lesha", "alena");
//        System.out.println(familyDB.countFamiliesWithMemberNumber(5));
//        familyDB.deleteAllChildrenOlderThen(-1);
//        System.out.println(familyDB.countFamiliesWithMemberNumber(5));
//        smist.setPet(pets);
//        smist.bornChild();
//        smist.bornChild();
//        familyDB.displayAllFamilies();
//        familyDB.getFamilyByIdAndDisplay(1);
//        smist.prettyFormat();
//        father.prettyFormat();
//        dog.prettyFormat();

        ConsoleFamilysControl myConsole = new ConsoleFamilysControl();
        myConsole.consoleWriter();
    }
}

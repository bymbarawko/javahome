package com.company.family;

import com.company.human.Human;
import com.company.pet.Pet;

import java.util.HashSet;
import java.util.List;

public class FamilyController {
    FamilyService FamilyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return FamilyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        FamilyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return FamilyService.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count) {
        return FamilyService.getFamiliesLessThan(count);
    }

    public int countFamiliesWithMemberNumber(int count) {
        return FamilyService.countFamiliesWithMemberNumber(count);
    }

    public void createNewFamily(Human parent1, Human parent2) {
        FamilyService.createNewFamily(parent1, parent2);
    }

    public Family getFamilyById (int index) {
        return FamilyService.getFamilyById (index);
    }

    public void getFamilyByIdAndDisplay (int index) {
        FamilyService.getFamilyById (index).prettyFormat();
    }

    public void deleteFamilyByIndex(int index) {
        FamilyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String nameM, String nameW) {
        return FamilyService.bornChild(family, nameM, nameW);
    }

    public Family adoptChild(Family family, Human child) {
        return FamilyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(long year) {
        FamilyService.deleteAllChildrenOlderThen(year);
    }

    public int count() {
        return FamilyService.count();
    }

    public void deleteFamilyByFamily(Family family) {
        FamilyService.deleteFamilyByFamily(family);
    }

    public void saveFamily(Family family) {
        FamilyService.saveFamily(family);
    }

    public HashSet<Pet> getPets(int index) {
        return FamilyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        FamilyService.addPet(index, pet);
    }

}

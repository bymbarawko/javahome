package com.company.family;

import com.company.pet.Pet;
import com.company.human.Human;
import com.company.human.HumanCreator;
import com.company.human.Man;
import com.company.human.Woman;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class Family implements HumanCreator {
    private Human mother;
    private Human father;
    private ArrayList<Human> children = new ArrayList<>();
    private HashSet<Pet> pet = new HashSet<>();

    static {
        System.out.println("загружаеться класс Family");
    }

    {
        System.out.println("Family: новый экземпляр");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }


    //getters
    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public ArrayList<Human> getChildren() {
        return this.children;
    }

    public HashSet<Pet> getPet() {
        return this.pet;
    }

    //setters
    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public void setPet(HashSet<Pet> pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        children.add(child);
    }

    public void addPet(Pet pet) {
        this.pet.add(pet);
    }

    public boolean deleteChild(int index) {
        boolean deletey = false;
        if(children != null ) {
            if(children.size() >= index) {
                children.remove(index);
                deletey = true;
            }
        }

        return deletey;
    }

    public void deleteChildByLink(Human deleted) {
        children.removeIf(human -> human == deleted);
    }

    public int countFamily() {
        if(children != null) {
            if(children.size() > 0) {
                return children.size() + 2;
            }
        }
        return 2;
    }

    public void prettyFormat() {
        System.out.println("family: ");
        System.out.println("    " + "Mother: " + this.mother);
        System.out.println("    " + "Father: " + this.father);
        System.out.println("    " + "Children: ");
        this.children
                .forEach(e -> System.out.println("    " + "    " + e));
        System.out.println("    " + "Pets: ");
        this.pet
                .forEach(e -> System.out.println("    " + "    " + e));
    }

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 17;
        result = prime * result + mother.hashCode();
        result = prime * result + father.hashCode();
        result = prime * result + children.hashCode();
        result = prime * result + pet.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Family other = (Family) obj;
        if (mother != other.mother)
            return false;
        if (father != other.father)
            return false;
        if(children.toArray().length != other.children.toArray().length){
            return false;}
        for(int i = 0; i < children.toArray().length; i++){
            if(children.toArray()[i] != other.children.toArray()[i]){
                return false;
            }
        }
        if (pet != other.pet)
            return false;
        return true;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("дален обьект типа Family: mother: " + mother +
                " father: " + father + " ");
        super.finalize();
    }

    @Override
    public String toString() {
            return "mother: " + mother + '\'' +
                    ", father: " + father + '\'' +
                    ", childrens: " + children +
                    ", pets: " + pet;
    }

    @Override
    public Human bornChild() {
        Random random = new Random();
        Human child;

        if (random.nextInt(2) == 1) {
            child = new Man(HumanCreator.nameM[random.nextInt(3)], father.getSurname(), System.currentTimeMillis(), (mother.getId() + father.getId()) / 2);
        }
        else {
            child = new Woman(HumanCreator.namew[random.nextInt(3)], father.getSurname(), System.currentTimeMillis(), (mother.getId() + father.getId()) / 2);
        }

        children.add(child);

        return child;
    }
}

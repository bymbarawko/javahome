package com.company.family;


import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }


    @Override
    public Family getFamilyByIndex (int index) {
        return familyList.get(index);
    }



    @Override
    public boolean deleteFamily(int index) {
        try {
            familyList.remove(index);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        return familyList.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        int in = familyList.indexOf(family);
        if(in > -1){
            familyList.set(in, family);
        }
        else {
            familyList.add(family);
        }
    }
}

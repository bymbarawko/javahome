package com.company.family;

import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;
import com.company.pet.Pet;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {
    CollectionFamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies()
                .forEach(Family::prettyFormat);
    }


    public List<Family> getFamiliesBiggerThan(int count) {
        return familyDao.getAllFamilies().stream()
                .filter(e -> e.countFamily() > count)
                .collect(Collectors.toList());
    }


    public List<Family> getFamiliesLessThan(int count) {
        return familyDao.getAllFamilies().stream()
                .filter(e -> e.countFamily() < count)
                .collect(Collectors.toList());
    }


    public int countFamiliesWithMemberNumber(int count) {
        return (int) familyDao.getAllFamilies().stream()
                .filter(e -> e.countFamily() == count)
                .count();
    }


    public void createNewFamily(Human parent1, Human parent2) {
        Family family = new Family(parent1, parent2);
        familyDao.saveFamily(family);
    }


    public Family getFamilyById (int index) {
        return familyDao.getFamilyByIndex (index);
    }


    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamily(index);
    }

    //оставил чтоб было)
    public void deleteFamilyByFamily(Family family) {
        familyDao.deleteFamily(family);
    }


    public Family bornChild(Family family, String nameM, String nameW) throws FamilyOverflowException {
        if(family.getChildren().size() >= 2){
            throw new FamilyOverflowException("этой семье будет много больше 2 детей.");
        }
        Human child;
        Random random = new Random();
        if(random.nextInt(2) == 1){
            child = new Man();
            child.setName(nameM);
        }
        else {
            child = new Woman();
            child.setName(nameW);
        }
        child.setBirthDate(System.currentTimeMillis());
        child.setSurname(family.getFather().getSurname());
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }


    public Family adoptChild(Family family, Human child) throws FamilyOverflowException {
        if(family.getChildren().size() >= 2){
            throw new FamilyOverflowException("этой семье будет много больше 2 детей.");
        }
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }


    public void deleteAllChildrenOlderThen(long year) {
        familyDao.getAllFamilies()
                .forEach(e -> e.getChildren().removeIf(human -> human.getBirthDate() > year));
    }


    public int count() {
        return familyDao.getAllFamilies().size();
    }



    public HashSet<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPet();
    }


    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.addPet(pet);
        saveFamily(family);

    }





//for test)
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }


}

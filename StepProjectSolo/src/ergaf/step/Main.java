package ergaf.step;

import ergaf.step.console.ConsoleMain;

public class Main {

    public static void main(String[] args) {
        ConsoleMain console = new ConsoleMain();
        console.startConsole();
    }
}

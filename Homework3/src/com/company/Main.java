package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[][] scedule = new String[7][2];
        boolean exit = false;

        scedule[0][0] = "sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "tuesday";
        scedule[2][1] = "покормить кота";
        scedule[3][0] = "wednesday";
        scedule[3][1] = "покормить кота наконец!";
        scedule[4][0] = "thursday";
        scedule[4][1] = "что бы сюда вставить?";
        scedule[5][0] = "friday";
        scedule[5][1] = "ня";
        scedule[6][0] = "saturday";
        scedule[6][1] = "наконец это закончилось";


        while (!exit) {
            System.out.print("Please, input the day of the week: ");
            String[] userIn = in.nextLine().toLowerCase().split(" ", 0);
            if(userIn.length != 0) {
                String userIntStr0 = userIn[0];
                String searchResult0 = scedule[getIndexDay(userIn[0], scedule)][0];
                if(getIndexDay(userIn[0], scedule) != 0) {
                    if(userIntStr0.equals(searchResult0)) {
                        System.out.println(scedule[getIndexDay(userIn[0], scedule)][1]);
                    }
                    else System.out.println("error 404)");
                }
                else {
                    if (userIn.length > 1) {
                        String userIntStr1 = userIn[1];
                        String searchResult1 = scedule[getIndexDay(userIn[1], scedule)][0];
                        if (userIntStr1.equals(searchResult1)) {
                            switch (userIn[0]) {
                                case "change":
                                case "reschedule":
                                    int chargeInd = getIndexDay(userIn[1], scedule);
                                    System.out.print("Please, input new tasks for "+scedule[chargeInd][0]+": ");
                                    scedule[chargeInd][1] = in.nextLine();
                                    break;
                                default:
                                    System.out.println("error 404)");
                            }
                        }
                        else System.out.println("редактирование: второе слово не найдено в массиве дней");
                    }
                    else System.out.println("редактирование: массив меньше 1");
                }
            }
            else {
                System.out.println("error 404)");
            }
        }
    }

    private static int getIndexDay(String day, String[][] schedule) {
        int response = 0;
        for (int i = 0; i < schedule.length; i++) {
            int temp = schedule[i][0].toLowerCase().indexOf(day.toLowerCase());
            if (temp >= 0) {
                response = i;
            }
        }
        return response;
    }

}

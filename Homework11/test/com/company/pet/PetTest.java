package com.company.pet;

import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class PetTest {

    @Test
    public void testHashCode() {
        Pet pet1 = new Dog();
        Pet pet2 = new Dog();
        HashSet<String> hab1 = new HashSet<>();
        HashSet<String> hab2 = new HashSet<>();
        hab1.add("1");
        hab1.add("3");
        hab2.add("2");
        hab2.add("4");
        pet1.setHabits(hab1);
        pet2.setHabits(hab2);

        assertNotEquals(pet1.hashCode(), pet2.hashCode());
        assertEquals(pet1.hashCode(), pet1.hashCode());
        assertEquals(pet2.hashCode(), pet2.hashCode());
    }

    @Test
    public void testEquals() {
        Pet pet1 = new Dog();
        Pet pet2 = new Dog();
        HashSet<String> hab1 = new HashSet<>();
        HashSet<String> hab2 = new HashSet<>();
        hab1.add("1");
        hab1.add("3");
        hab2.add("2");
        hab2.add("4");
        pet1.setHabits(hab1);
        pet2.setHabits(hab2);


        assertFalse(pet1.equals(pet2));
    }
}
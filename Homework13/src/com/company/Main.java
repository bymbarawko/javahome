package com.company;


import com.company.family.*;
import com.company.pet.Pet;
import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    static {
        System.out.println("загружаеться класс Main");
    }

    {
        System.out.println("Main: новый экземпляр");
    }


    public static void main(String[] args) {
        ConsoleFamilysControl myConsole = new ConsoleFamilysControl();
        myConsole.consoleWriter();
    }
}

package com.company.family;


import com.company.Logger;

import java.io.*;
import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList = new ArrayList<>();
    Logger logger = new Logger();

    @Override
    public List<Family> getAllFamilies() {
        logger.log(LogOrError.LOG, "Получение списка всех семей");
        return familyList;
    }


    @Override
    public Family getFamilyByIndex (int index) {
        logger.log(LogOrError.LOG, "Получение конкретной семьи");
        return familyList.get(index);
    }



    @Override
    public boolean deleteFamily(int index) {
        logger.log(LogOrError.LOG, "Получение удаление семьи");
        try {
            familyList.remove(index);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        logger.log(LogOrError.LOG, "Получение удаление семьи");
        return familyList.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        logger.log(LogOrError.LOG, "Сохранения семьи");
        int in = familyList.indexOf(family);
        if(in > -1){
            familyList.set(in, family);
        }
        else {
            familyList.add(family);
        }
    }

    public void saveData () {
        File file = new File("Data");
        try {
            FileOutputStream f = new FileOutputStream(file);
            ObjectOutputStream o = new ObjectOutputStream(f);

            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            for (Family family : familyList) {
                o.writeObject(family);
            }

            o.close();
            f.close();

        } catch (IOException e ) {
            e.printStackTrace();
            logger.log(LogOrError.ERROR, "ошибка записи в файл базы данных");
        }
    }

    @Override
    public void loadData() {
        File file = new File("Data");
        try {
            FileInputStream f = new FileInputStream(file);
            ObjectInputStream o = new ObjectInputStream(f);

            while(f.available() > 0) {
                familyList.add((Family) o.readObject());
            }

            o.close();
            f.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            logger.log(LogOrError.ERROR, "ошибка чтения из файла базы данных");
        }
    }

    public void clearData() {
        familyList.clear();
        System.out.println("Данные о семьях очищены.");
        logger.log(LogOrError.LOG, "Данные о семьях очищены.");
    }
}

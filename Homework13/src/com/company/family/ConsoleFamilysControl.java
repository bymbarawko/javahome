package com.company.family;

import com.company.human.Human;
import com.company.pet.Dog;
import com.company.pet.Pet;

import java.util.Scanner;

public class ConsoleFamilysControl {
    FamilyController controller = new FamilyController();
    Scanner scan = new Scanner(System.in);
    ConsoleMenu menu = new ConsoleMenu(controller);

    public void consoleWriter() {
        int userInt = -1;
        boolean exit = true;
        while(exit) {
            menu.printMainMenu();
            String userWrite = scan.nextLine();
            if(userWrite.toLowerCase().equals("exit")){
                break;
            }
            userInt = doneInt(userWrite);
            switch (userInt) {
                case 1:
                    testCreate();
                    break;
                case 2:
                    controller.displayAllFamilies();
                    ifMenu();
                    break;
                case 3:
                    menu.menu3GetFamiliesBiggerThan();
                    ifMenu();
                    break;
                case 4:
                    menu.menu4GetFamiliesLessThan();
                    ifMenu();
                    break;
                case 5:
                    menu.menu5CountFamiliesWithMemberNumber();
                    ifMenu();
                    break;
                case 6:
                    menu.menu6CreateFamily();
                    ifMenu();
                    break;
                case 7:
                    menu.menu7DeleteFamilyByIndex();
                    ifMenu();
                    break;
                case 8:
                    try{
                        menu.printMenu8EditFamilyByIndex();
                    } catch (FamilyOverflowException e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 9:
                    menu.deleteAllChildrenOlderThen();
                    ifMenu();
                    break;
                case 10:
                    controller.saveData();
                    ifMenu();
                    break;
                case 11:
                    controller.loadData();
                    ifMenu();
                    break;
                case 12:
                    controller.clearData();
                    ifMenu();
                    break;
            }

        }
    }

    public static boolean isNumeric(String strNum) {
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            System.out.println("введенное не являеться целым числом)");
            return false;
        }
        return true;
    }

    public static int doneInt (String playerIn) {
        if (isNumeric(playerIn)) {
            return Integer.parseInt(playerIn);
        }
        return -1;
    }

    void testCreate() {
        Human alisa = new com.company.human.Woman("alisa", "̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞", "11/11/2001", 95);
        Human softwareArchitectForJava = new com.company.human.Human("ivan", "̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̯̰̦̭̠̟̪̹̲̜̺̫͈̳̗̰̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅̅͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͜͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞͞", "12/02/1578", 200);

        Human mother = new com.company.human.Woman("Vasilisa", "levchenko", "20/11/1996", 70);
        Human father = new com.company.human.Man("Sasha", "levchenko", "11/02/1994", 90);
        Pet dog = new Dog("хороший мальчик");

        Family smits = new Family(alisa, softwareArchitectForJava);
        Family simpsons = new Family(mother, father);
        simpsons.bornChild();
        simpsons.bornChild();
        simpsons.addPet(dog);
        controller.saveFamily(smits);
        controller.saveFamily(simpsons);
    }

    void ifMenu() {
        System.out.print("что бы вернуться в главное меню нажмите Ентер");
        scan.nextLine();
    }
}

package com.company.family;

import com.company.human.Human;
import com.company.human.Man;
import com.company.human.Woman;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;

import static com.company.family.ConsoleFamilysControl.doneInt;
import static com.company.family.ConsoleFamilysControl.isNumeric;

public class ConsoleMenu {
    Scanner scan = new Scanner(System.in);
    FamilyController controller;

    ConsoleMenu(FamilyController controller){
        this.controller = controller;
    }

    //main
    void printMainMenu() {
        System.out.println("- 1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)");
        System.out.println("- 2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)");
        System.out.println("- 3. Отобразить список семей, где количество людей больше заданного");
        System.out.println("- 4. Отобразить список семей, где количество людей меньше заданного");
        System.out.println("- 5. Подсчитать количество семей, где количество членов равно");
        System.out.println("- 6. Создать новую семью");
        System.out.println("- 7. Удалить семью по индексу семьи в общем списке");
        System.out.println("- 8. Редактировать семью по индексу семьи в общем списке ");
        System.out.println("- 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)");
        System.out.println("- 10. Сохранить данные из базы программы в файл ");
        System.out.println("- 11. Загрузить данные из файла в базу программы ");
        System.out.println("- 12. Очистить базу данных программы ");
        System.out.print("Введите номер действия: ");
    }
    //конец main
    //3
    void menu3GetFamiliesBiggerThan(){
        System.out.println("Введите количество людей для отображения семей где людей больше: ");
        int userI = doneInt(scan.nextLine());
        if(userI > -1)
            System.out.println(controller.getFamiliesBiggerThan(userI));
    }
    //конец 3
    //4
    void menu4GetFamiliesLessThan(){
        System.out.println("Введите количество людей для отображения семей где людей меньше: ");
        int userI = doneInt(scan.nextLine());
        if(userI > -1)
            System.out.println(controller.getFamiliesLessThan(userI));
    }
    //конец 4
    //5
    void menu5CountFamiliesWithMemberNumber(){
        System.out.println("Введите количество людей для отображения семей с этим кол-вом: ");
        int userI = doneInt(scan.nextLine());
        if(userI > -1)
            System.out.println("колличество семей где колличество человек "+userI+" = "+ controller.countFamiliesWithMemberNumber(userI));
    }
    //конец 5
    //6 длинное, начало
    void menu6CreateFamily(){
        System.out.println("Введите имя матери: ");
        String motherName = scan.nextLine();
        System.out.println("Введите фамилию матери: ");
        String motherSurname = scan.nextLine();
        String motherDateOfBird = getValidateDateOfBirdForMenu6("матери");
        System.out.println("Введите айкью рождения матери: ");
        int motherIq = doneInt(scan.nextLine());
        Woman mother = new Woman(motherName, motherSurname, motherDateOfBird, motherIq);

        System.out.println("Введите имя отца: ");
        String fatherName = scan.nextLine();
        System.out.println("Введите фамилию отца: ");
        String fatherSurname = scan.nextLine();
        String fatherDateOfBird = getValidateDateOfBirdForMenu6("отца");
        System.out.println("Введите айкью рождения отца: ");
        int fatherIq = doneInt(scan.nextLine());
        Man father = new Man(fatherName, fatherSurname, fatherDateOfBird, fatherIq);

        Family family = new Family(mother, father);
        controller.saveFamily(family);
    }

    String getValidateDateOfBirdForMenu6(String sex) {
        boolean months = true;
        boolean day = true;
        int monthsOfBird = -1;
        int dayOfBird = -1;
        System.out.println("Введите год рождения "+sex+": ");
        int YearOfBird = doneInt(scan.nextLine());
        while(months) {
            System.out.println("Введите валидный месяц рождения "+sex+": ");
            monthsOfBird = doneInt(scan.nextLine());
            if(monthsOfBird < 13 && monthsOfBird > 0){
                months = false;
            } else {
                System.out.print("Вы ввели несуществующий месяц, ");
            }
        }
        int countDayInThisMonth = getMaxDayOfMonthForMenu6(YearOfBird, monthsOfBird-1);
        while (day) {
            System.out.println("Введите день рождения "+sex+": ");
            dayOfBird = doneInt(scan.nextLine());
            if (dayOfBird <= countDayInThisMonth && dayOfBird > 0){
                day = false;
                System.out.println(countDayInThisMonth);
            } else {
                System.out.print("Вы ввели больше дней чем есть в месяце, ");
                System.out.println(countDayInThisMonth);
            }
        }
        return dayOfBird + "/" + monthsOfBird + "/" + YearOfBird;
    }

    int getMaxDayOfMonthForMenu6(int year, int month) {
        Calendar myCalendar = (Calendar) Calendar.getInstance().clone();
        myCalendar.set(year, month, 1);
        return myCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    //конец 6
    //7
    void menu7DeleteFamilyByIndex(){
        System.out.println("Введите индекс семьи которая будет удалена: ");
        int userI = doneInt(scan.nextLine());
        if(userI > -1){
            controller.deleteFamilyByIndex(userI);
            System.out.println("Семья успешно удалена!");
        }
    }
    //конец 7
    //8
    void printMenu8EditFamilyByIndex() throws FamilyOverflowException {
        if(controller.getAllFamilies().size() <= 0) {
            System.out.println("У вас в базе нет ни одной семьи.");
            String menu = scan.nextLine();
        } else {
            System.out.println("- 1. Родить ребенка");
            System.out.println("- 2. Усыновить ребенка");
            System.out.println("- 3. Вернуться в главное меню");
            System.out.print("Введите номер действия: ");
            validationNumber();
        }
    }

    void validationNumber() throws FamilyOverflowException {
        int userI = doneInt(scan.nextLine());
        while (userI < 0 || userI > 3){
            System.out.print("Введите число от 1 до 3: ");
            userI = doneInt(scan.nextLine());
        }
        switch (userI) {
            case 1:
                ifNumber1();
                break;
            case 2:
                ifNumber2();
                break;
            case 3:
                break;
        }
    }
    void ifNumber1() {
        System.out.print("Введите индекс семьи: ");
        int index = doneInt(scan.nextLine());
        while (index < 0 || index > controller.getAllFamilies().size()-1){
            System.out.print("Введите положительное число или 0, не больше длинны массива семей: ");
            index = doneInt(scan.nextLine());
        }
        Family thiss = controller.getFamilyById(index);
        System.out.print("Введите какое имя дать мальчику: ");
        String manName = scan.nextLine();
        System.out.print("Введите какое имя дать девочке: ");
        String womanName = scan.nextLine();
        controller.bornChild(thiss, manName, womanName);
    }
    void ifNumber2() {
        System.out.print("Введите индекс семьи: ");
        int index2 = doneInt(scan.nextLine());
        while (index2 < 0 || index2 > controller.getAllFamilies().size()-1){
            System.out.print("Введите положительное число или 0, не больше длинны массива семей: ");
            index2 = doneInt(scan.nextLine());
        }
        Family thiss2 = controller.getFamilyById(index2);
        System.out.print("Введите имя ребенка: ");
        String childName = scan.nextLine();
        System.out.print("Введите фамилию ребенка: ");
        String childSurname = scan.nextLine();
        System.out.print("Введите год рождения ребенка: ");
        String childBirdDate = scan.nextLine();
        while (!isNumeric(childBirdDate)) {
            System.out.print("Введите год рождения ребенка числом!: ");
            childBirdDate = scan.nextLine();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date1 = new Date();
        try {
            date1 =  simpleDateFormat.parse(String.valueOf(childBirdDate));
        } catch (java.text.ParseException ignored) {
        }
        long childBirdDateDone = date1.getTime();
        System.out.print("Введите IQ ребенка: ");
        int iq = doneInt(scan.nextLine());
        while (iq < 0) {
            System.out.print("Введите IQ ребенка которое больше 0!: ");
            iq = doneInt(scan.nextLine());
        }
        System.out.print(iq);
        Human child = new Human(childName, childSurname, childBirdDateDone, iq);
        thiss2.addChild(child);
        controller.saveFamily(thiss2);
    }
    //конец 8
    //9
    void deleteAllChildrenOlderThen() {
        System.out.print("Введите возраст по достижению которого дети удаляються этим методом: ");
        long age = doneInt(scan.nextLine());
        while (age < 0) {
            System.out.print("Введите возраст рождения ребенка числом!: ");
            age = doneInt(scan.nextLine());
        }
        long ageLong = age*365*24*60*60*1000;
        controller.deleteAllChildrenOlderThen(ageLong);
    }

    //конец 9
}

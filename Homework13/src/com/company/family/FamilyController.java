package com.company.family;

import com.company.Logger;
import com.company.human.Human;
import com.company.pet.Pet;

import java.util.HashSet;
import java.util.List;

public class FamilyController {
    FamilyService familyService = new FamilyService();
    Logger logger = new Logger();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return familyService.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count) {
        return familyService.getFamiliesLessThan(count);
    }

    public int countFamiliesWithMemberNumber(int count) {
        return familyService.countFamiliesWithMemberNumber(count);
    }

    public void createNewFamily(Human parent1, Human parent2) {
        familyService.createNewFamily(parent1, parent2);
    }

    public Family getFamilyById (int index) {
        return familyService.getFamilyById (index);
    }

    public void getFamilyByIdAndDisplay (int index) {
        familyService.getFamilyById (index).prettyFormat();
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String nameM, String nameW) {
        try{
            return familyService.bornChild(family, nameM, nameW);
        } catch (FamilyOverflowException e){
            System.out.println("ребенок не был добавлен ибо превышен лимит на кол-во детей.");
            logger.log(LogOrError.ERROR, "ребенок не был добавлен ибо превышен лимит на кол-во детей.");
            return family;
        }
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(long year) {
        familyService.deleteAllChildrenOlderThen(year);
    }

    public int count() {
        return familyService.count();
    }

    public void deleteFamilyByFamily(Family family) {
        familyService.deleteFamilyByFamily(family);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public HashSet<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void saveData() {
        familyService.saveData();
    }

    public void loadData() {
        familyService.loadData();
    }

    public void clearData() {
        familyService.clearData();
    }

}

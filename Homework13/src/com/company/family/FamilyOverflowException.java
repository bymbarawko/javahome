package com.company.family;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String message){
        super(message);

    }
}

package com.company;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits = new String[5];
    Pet(){

    }
    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    void eat() {
        System.out.println("Я кушаю!");
    }
    void respond() {
        System.out.println("Привет, хозяин. Я - "+nickname+". Я соскучился!");
    }
    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return species+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
//у класса Pet должен выводить сообщение следующего вида: dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}, где dog - вид животного;

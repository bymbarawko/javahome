package com.company;

public class Main {

    public static void main(String[] args) {
        Pet dog = new Pet("dog", "хороший мальчик", 10, 60, new String[] {"sleep", "eat"});
        Pet cat = new Pet("cat", "murzik");
        Pet beast = new Pet();

        Human bitard = new Human("nagibator228", "ivanov", 15, 1);
        Human vasia = new Human("ivan", "ivanov", 20, 80, new Human(), new Human());

        Human mother = new Human();
        Human father = new Human();
        Human sasha = new Human("Sasha", "lenivii", 18, 100, dog, mother, father, new String[][] {{"пн", "ср"},{"пожрать","поспать"}});

        bitard.greetPet();
        bitard.describePet();
        System.out.println(bitard.toString());

        vasia.greetPet();
        vasia.describePet();
        System.out.println(vasia.toString());

        sasha.greetPet();
        sasha.describePet();
        System.out.println(sasha.toString());

        mother.greetPet();
        mother.describePet();
        System.out.println(mother.toString());

        dog.eat();
        dog.respond();
        dog.foul();
        System.out.println(dog.toString());

        cat.eat();
        cat.respond();
        cat.foul();
        System.out.println(cat.toString());

        beast.eat();
        beast.respond();
        beast.foul();
        System.out.println(beast.toString());
    }
}

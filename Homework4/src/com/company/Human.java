package com.company;


public class Human {
    String name;
    String surname;
    int year;
    int id;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    Human() {

    }

    Human(String name, String surname, int year, int id) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
    }

    Human(String name, String surname, int year, int id, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
        this.mother = mother;
        this.father = father;
    }

    Human(String name, String surname, int year, int id, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.id = id;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    void greetPet() {
        if (pet != null) {
            System.out.println("Привет, "+pet.nickname);
        }
        else {
            System.out.println("Привет, одиночество))");
        }
    }
    void describePet() {
        if (pet != null) {
            if (pet.trickLevel > 50) {
                System.out.println("У меня есть "+pet.species+", ему "+pet.age+" лет, он очень хитрый");
            }
            else {
                System.out.println("У меня есть "+pet.species+", ему "+pet.age+" лет, он почти не хитрый");
            }
        } else {
            System.out.println("У меня нет питомца.");
        }
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", id=" + id +
                ", mother=" + mother +
                ", father=" + father +" "+ "pet="+pet+"";
    }
}
